package com.github.zuihou.authority.service.common;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.zuihou.authority.entity.common.OptLog;

/**
 * <p>
 * 业务接口
 * 系统日志
 * </p>
 *
 * @author zuihou
 * @date 2019-07-02
 */
public interface OptLogService extends IService<OptLog> {

}
